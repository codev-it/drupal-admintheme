<?php

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_system_theme_settings_alter().
 *
 * @noinspection PhpUnused
 * @noinspection PhpUnusedParameterInspection
 */
function codev_admintheme_form_system_theme_settings_alter(&$form, FormStateInterface $form_state) {
  $foundation_settings = theme_get_setting('foundation') ?: [];

  $form['foundation'] = [
    '#type'  => 'details',
    '#title' => t('Foundation settings'),
    '#tree'  => TRUE,
  ];

  $form['foundation']['abide'] = [
    '#type'          => 'checkbox',
    '#title'         => t('Enable foundation abide function'),
    '#default_value' => $foundation_settings['abide'] ?? FALSE,
  ];
}
