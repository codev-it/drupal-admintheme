const fs = require('fs');
const {
  lookupPlaceholder,
  lookupPlaceholderMultiple
} = require('./placeholder');

// Build watch list object
const buildWatchList = (list) => {
  const watch = {};

  if (list instanceof Array) {
    const appendToWatchList = (key, val) => {
      if (watch[key] !== undefined) {
        watch[key].push(val);
      } else {
        watch[key] = [val];
      }
    };

    list.forEach((value) => {
      value = lookupPlaceholder(value);
      if (fs.existsSync(value)) {
        appendToWatchList('dirs', value);
      } else {
        appendToWatchList('files', value);
      }
    });
  } else if (list instanceof Object) {
    return lookupPlaceholderMultiple(list);
  }

  return watch;
};

// Build shell event object
const buildShellEvents = (list) => {
  let js = {};
  let scss = {};

  const buildEvent = (events, presets, override = true) => {
    Object.entries(events).forEach((event) => {
      const [key, val] = event;
      if (override) {
        presets[key] = val;
      } else if (!(key in presets)) {
        presets[key] = val;
      }
    });

    Object.entries(presets).forEach((preset) => {
      const [id, event] = preset;
      if (event instanceof Object) {
        Object.entries(Object(event)).forEach((data) => {
          const [key, value] = data;
          if (key === 'scripts' && value.length) {
            if (value instanceof Array) {
              presets[id][key] = lookupPlaceholderMultiple(value);
            } else {
              presets[id][key] = [lookupPlaceholder(value)];
            }
          }
        });
      }
    });

    return presets;
  };

  if (list instanceof Object && !(list instanceof Array)) {
    Object.entries(list).forEach((item) => {
      const [key, value] = item;
      switch (key) {
        case 'js':
          js = buildEvent(value, js);
          break;
        case 'scss':
          scss = buildEvent(value, scss);
          break;
        default:
          js = buildEvent(value, js, false);
          scss = buildEvent(value, scss, false);
          break;
      }
    });
  }

  return [js, scss];
};

module.exports = {
  buildWatchList,
  buildShellEvents
};
