const path = require('path');
const configLoader = require('./config-loader');

// Load config
const { SRC, DIST, PLACEHOLDER } = configLoader();

// Default placeholder function
const _defaultPlaceholder = (str) => {
  str = str.replace(/%root_path%/gm, path.resolve());
  // eslint-disable-next-line no-use-before-define
  str = str.replace(/%source_path%/gm, lookupPlaceholder(SRC || '', false));
  // eslint-disable-next-line no-use-before-define
  return str.replace(/%assets_path%/gm, lookupPlaceholder(DIST || '', false));
};

// Additional placeholder function
const _additionalPlaceholder = (str) => {
  if (!(PLACEHOLDER instanceof Array) && PLACEHOLDER instanceof Object) {
    Object.entries(PLACEHOLDER).forEach((item) => {
      const [key, val] = item;
      const regex = new RegExp(`%${key}%`, 'gm');
      str = str.replace(regex, val);
    });
  }
  return str;
};

// Resolver placeholder function
const _resolverPlaceholder = (str) => {
  let match;
  const regex = new RegExp('%path_resolve\\((.*?)\\)%', 'gm');
  do {
    match = regex.exec(str);
    if (match) {
      const [search, val] = match;
      str = str.replace(
        search,
        path.resolve(
          ...val.split(',').map((item) => {
            return item.trim();
          })
        )
      );
    }
  } while (match);
  return str;
};

// Placeholder lookup function
const lookupPlaceholder = (str, defaults = true) => {
  if (str === undefined) {
    return;
  }

  // defaults
  if (defaults) {
    str = _defaultPlaceholder(str);
  }

  // additional placeholder
  str = _additionalPlaceholder(str);

  // path resolver
  str = _resolverPlaceholder(str);

  return str;
};

// Placeholder lookup function multiple.
// Working with object, arrays and strings.
const lookupPlaceholderMultiple = (data, defaults = true) => {
  const mapLookupPlaceholder = (val) => {
    return lookupPlaceholder(val, defaults);
  };

  if (data instanceof Array) {
    data = data.map(mapLookupPlaceholder);
  } else if (data instanceof Object) {
    Object.entries(data).forEach((item) => {
      const [key, val] = item;
      if (val instanceof Array) {
        data[key] = val.map(mapLookupPlaceholder);
      } else {
        data[key] = lookupPlaceholder(val, defaults);
      }
    });
  } else if (data !== undefined) {
    data = lookupPlaceholder(data.toString(), defaults);
  }
  return data;
};

module.exports = {
  lookupPlaceholder,
  lookupPlaceholderMultiple
};
