// eslint-disable-next-line import/no-extraneous-dependencies
const purgecssFromHtml = require('purgecss-from-html');

// Remove html open close tags
const _removeHtmlOpenCloseTags = (str) => {
  str = str.replace(/<\//gm, ' ');
  str = str.replace(/</gm, ' ');
  return str.replace(/>/gm, ' ');
};

// Remove first or last quotes if exists
const _removeQuotes = (str) => {
  str = str.replace(/^['"]/gm, ' ');
  return str.replace(/['"]$/gm, ' ');
};

// Remove first or last quotes if exists
const _removeTwigOpenCloseTags = (str) => {
  str = str.replace(/}}/gm, ' ');
  str = str.replace(/%}/gm, ' ');
  str = str.replace(/{{/gm, ' ');
  return str.replace(/{%/gm, ' ');
};

// Jquery get function key value.
const _matchJqueryKeyValue = (fnName, content) => {
  let match;
  const ret = [];
  const regex = new RegExp(`${fnName}\\(['|"](.*?)['|"]?\\)`, 'gm');

  do {
    match = regex.exec(content);
    if (match && match[1] !== undefined && match[1].length) {
      let [key, val] = match[1].split(',');
      if (val !== undefined) {
        val = _removeQuotes(val.trim()).trim();
      }
      if (key !== undefined) {
        key = _removeQuotes(key.trim()).trim();
        ret.push({ key, val: val || '' });
      }
    }
  } while (match);

  return ret;
};

// Remove first or last quotes if exists
const _fmtTwigItemsArray = (arr) => {
  const ret = [];
  arr.forEach((val) => {
    val = _removeTwigOpenCloseTags(val);
    if (val.length && val !== ' ') {
      ret.push(val);
    }
  });
  return ret;
};

// Extract from text
const purgecssFromText = (content) => {
  let match;
  const arr = [];
  const arrFmt = [];
  const defaults = purgecssFromHtml(content);
  const regex = new RegExp(/'(.*?)'|"(.*?)"/, 'gm');

  do {
    match = regex.exec(content);
    if (match && match[1] !== undefined && match[1].length) {
      arr.push(...match[1].split(' '));
    }
  } while (match);

  arr.forEach((item) => {
    if (item.length) {
      const val = _removeHtmlOpenCloseTags(item).split(' ');
      val.forEach((v) => {
        if (v.length) {
          const vFmt = _removeQuotes(v).split('=');
          vFmt.forEach((str) => {
            if (str.length) {
              arrFmt.push(_removeQuotes(str).trim());
            }
          });
        }
      });
    }
  });

  if (arrFmt.length) {
    defaults.ids.push(...arrFmt);
    defaults.tags.push(...arrFmt);
    defaults.classes.push(...arrFmt);
    defaults.attributes.names.push(...arrFmt);
    defaults.attributes.values.push(...arrFmt);
  }
  return defaults;
};

// Extract from twig
const purgecssFromTwig = (content) => {
  const defaults = purgecssFromText(content);
  defaults.ids = _fmtTwigItemsArray(defaults.ids);
  defaults.tags = _fmtTwigItemsArray(defaults.tags);
  defaults.classes = _fmtTwigItemsArray(defaults.classes);
  defaults.attributes.names = _fmtTwigItemsArray(defaults.attributes.names);
  defaults.attributes.values = _fmtTwigItemsArray(defaults.attributes.values);
  return defaults;
};

// Extract from js
const purgecssFromJs = (content) => {
  const defaults = purgecssFromText(content);
  const dataMatch = _matchJqueryKeyValue('data', content);
  dataMatch.forEach((item) => {
    const { key, val } = item;
    defaults.attributes.names.push(`data-${key}`);
    if (val !== '') {
      defaults.attributes.values.push(val);
    }
  });
  return defaults;
};

module.exports = {
  purgecssFromText,
  purgecssFromTwig,
  purgecssFromJs
};
