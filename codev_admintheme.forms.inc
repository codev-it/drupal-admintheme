<?php

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: codev_admintheme.forms.inc
 * .
 */

/**
 * Implements hook_theme_suggestions_form_element_alter().
 *
 * @noinspection PhpUnused
 */
function codev_admintheme_theme_suggestions_form_alter(array &$suggestions, array $variables, $hook) {
  $elem = $variables['element'];
  $theme_keys = $elem['#theme'] ?? [];
  if (is_array($theme_keys)) {
    foreach ($theme_keys as $key) {
      $suggestions[] = sprintf('%s__%s', $hook, $key);
    }
  }
  else {
    $suggestions[] = sprintf('%s__%s', $hook, $theme_keys);
  }
  $suggestions = array_unique($suggestions);
}

/**
 * Implements hook_form_alter().
 *
 * @noinspection PhpUnused
 * @TODO         Rework abide
 */
function codev_admintheme_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  //  Set abide forms
  if (_is_foundation_abide_enable()) {
    if (in_array($form_id, [
        'user_form',
        'user_register_form',
        'user_role_form',
        'user_admin_settings',
        'taxonomy_vocabulary_form',
        'menu_add_form',
        'menu_edit_form',
        'path_alias_form',
        'redirect_redirect_form',
        'redirect_redirect_edit_form',
      ]) || $form_state->getFormObject() instanceof ConfigFormBase) {
      $form['#attributes']['data-abide'] = '';
      $form['#attributes']['novalidate'] = TRUE;
    }
  }
}

/**
 * Implements hook_form_BASE_FORM_ID_alter() for menu_link_edit.
 *
 * @noinspection PhpUnused
 */
function codev_admintheme_form_menu_link_edit_alter(&$form, FormStateInterface $form_state) {
  _codev_admintheme_link_content($form, $form_state);
}

/**
 * Implements hook_form_BASE_FORM_ID_alter() for menu_link_content_form.
 *
 * @noinspection PhpUnused
 */
function codev_admintheme_form_menu_link_content_form_alter(array &$form, FormStateInterface $form_state) {
  _codev_admintheme_link_content($form, $form_state);
}

/**
 * Link edit and add sidebar content builder.
 *
 * @param array                                $form
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *
 * @noinspection PhpUnusedParameterInspection
 */
function _codev_admintheme_link_content(array &$form, FormStateInterface $form_state) {
  // Set abide forms
  if (_is_foundation_abide_enable()) {
    $form['#attributes']['data-abide'] = '';
    $form['#attributes']['novalidate'] = TRUE;
  }

  // set main layout container
  $form['content'] = [
    '#type'       => 'container',
    '#weight'     => -50,
    '#attributes' => [
      'class' => 'content',
    ],
  ];

  $form['sidebar'] = [
    '#type'       => 'container',
    '#weight'     => -49,
    '#attributes' => [
      'class' => [
        'sidebar',
        'entity-meta',
      ],
    ],
  ];

  // make sure the display settings container is on the top
  if (!empty($form['display_settings'])) {
    $form['display_settings']['#weight'] = -50;
  }

  // include fa menu icon picker to sidebar
  if (!empty($form['fa_icon'])) {
    $form['link_icon'] = [
      '#type'   => 'details',
      '#title'  => t('Link icon'),
      '#group'  => 'sidebar',
      '#weight' => 99,
    ];
    $form['link_icon']['fa_icon'] = $form['fa_icon'];
    unset($form['fa_icon']);
  }

  // include options from sum custom modules if exists
  if (!empty($form['options'])) {
    $form['options']['#type'] = 'container';
    $form['options']['#group'] = 'sidebar';
  }

  // split field into regions
  foreach ($form as $key => $elem) {
    if (is_array($elem) && isset($elem['#type'])
      && !in_array($key, ['content', 'sidebar'])
      && !in_array($elem['#type'], ['token', 'hidden', 'actions'])) {
      $group = $elem['#group'] ?? 'content';
      if ($group == 'sidebar') {
        $form['sidebar'][$key] = $elem;
      }
      else {
        $form['content'][$key] = $elem;
      }
      unset($form[$key]);
    }
  }
  $form['content']['actions'] = $form['actions'];
  unset($form['actions']);
}

/**
 * Implements hook_form_BASE_FORM_ID_alter() for node_form.
 *
 * @noinspection PhpUnused
 * @noinspection PhpUnusedParameterInspection
 */
function codev_admintheme_form_node_form_alter(&$form, FormStateInterface $form_state) {
  // @TODO Implement Abide

  // Set field weights
  if (!empty($form['meta'])) {
    $form['meta']['#weight'] = -99;
  }
  if (!empty($form['author'])) {
    $form['author']['#weight'] = -98;
  }
  if (!empty($form['menu'])) {
    $form['menu']['#weight'] = -97;
  }
}

/**
 * Implements hook_form_BASE_FORM_ID_alter() for taxonomy_term_form.
 *
 * @noinspection PhpUnused
 * @noinspection PhpUnusedParameterInspection
 */
function codev_admintheme_form_block_content_form_alter(&$form, FormStateInterface $form_state) {
  // Set abide forms
  if (_is_foundation_abide_enable()) {
    $form['#attributes']['data-abide'] = '';
    $form['#attributes']['novalidate'] = TRUE;
  }
}

/**
 * Implements hook_form_BASE_FORM_ID_alter() for media_form.
 *
 * @noinspection PhpUnused
 * @noinspection PhpUnusedParameterInspection
 */
function codev_admintheme_form_taxonomy_term_form_alter(&$form, FormStateInterface $form_state) {
  // Set abide forms
  if (_is_foundation_abide_enable()) {
    $form['#attributes']['data-abide'] = '';
    $form['#attributes']['novalidate'] = TRUE;
  }
}

/**
 * Implements hook_form_BASE_FORM_ID_alter() for media_form.
 *
 * @noinspection PhpUnused
 * @noinspection PhpUnusedParameterInspection
 */
function codev_admintheme_form_media_form_alter(&$form, FormStateInterface $form_state) {
  // Set abide forms
  if (_is_foundation_abide_enable()) {
    $form['#attributes']['data-abide'] = '';
    $form['#attributes']['novalidate'] = TRUE;
  }
}

/**
 * Return the foundation abide function status.
 */
function _is_foundation_abide_enable(): bool {
  $foundation_settings = theme_get_setting('foundation') ?: [];
  return boolval($foundation_settings['abide'] ?? FALSE);
}
