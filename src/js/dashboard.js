(($, Drupal) => {
  const { behaviors, ajax } = Drupal;

  const Dashboard = {
    /**
     * Opened status for widget select list.
     */
    _opened: false,

    /**
     * Fade the widget select list.
     */
    fadeToggle() {
      const $widgetSelectWrp = $('#widgets-select-wrp').once();
      if ($widgetSelectWrp.length) {
        const animationSpeed = 1000;
        const $widgetSelect = $widgetSelectWrp.find('#widgets-select');
        const $addNewWidgetButton = $widgetSelectWrp.find(
          '.add-new-widgets-button'
        );
        const $closeWidgetSelectButton = $widgetSelectWrp.find(
          '.widgets-select-close-button'
        );

        $addNewWidgetButton.on('click', (event) => {
          event.preventDefault();
          $widgetSelect.stop();
          this._opened = $widgetSelectWrp.hasClass('open');
          if (this._opened) {
            $widgetSelect.animate({ height: '0' }, animationSpeed, () => {
              this._toggleOpenCloseState($widgetSelectWrp);
            });
          } else {
            this._appendWidgetSelectList(
              $widgetSelect.find('#widgets-list'),
              $addNewWidgetButton.attr('href')
            ).then(() => {
              this._slickSlider($widgetSelect.find('#widgets'));
              // eslint-disable-next-line max-nested-callbacks
              $widgetSelect.animate({ height: '200px' }, animationSpeed, () => {
                this._toggleOpenCloseState($widgetSelectWrp);
              });
            });
          }
        });

        $closeWidgetSelectButton.on('click', () => {
          $widgetSelect.animate({ height: '0' }, animationSpeed, () => {
            this._toggleOpenCloseState($widgetSelectWrp);
          });
        });
      }
    },

    /**
     * Toggle open close state for element.
     *
     * @param {Object} $elem Element to toggle classes.
     * @private
     */
    _toggleOpenCloseState($elem) {
      if (this._opened) {
        this._opened = false;
        $elem.addClass('close').removeClass('open');
      } else {
        this._opened = true;
        $elem.addClass('open').removeClass('close');
      }
    },

    /**
     * Append the current widget to select list.
     *
     * @param {Object} $elem Element to append the list
     * @param {string} url Select list url to load
     * @return {*} Ajax handler
     * @private
     */
    _appendWidgetSelectList($elem, url) {
      const ajaxReq = ajax({
        url,
        progress: { type: 'fullscreen' }
      });
      ajaxReq.commands.insert = (event, response) => {
        $elem.html(response.data);
      };
      return ajaxReq.execute();
    },

    /**
     * Slick slider.
     *
     * @param {Object} $elem Slick slider element
     * @private
     */
    _slickSlider($elem) {
      if ($elem.find('> *').length) {
        // noinspection JSUnresolvedFunction
        $elem.slick({
          prevArrow:
            '<button class="slick-prev" aria-label="Previous" type="button"><i class="fas fa-caret-left"></i></button>',
          nextArrow:
            '<button class="slick-next" aria-label="Next" type="button"><i class="fas fa-caret-right"></i></button>',
          infinite: false,
          slidesToShow: 4,
          slidesToScroll: 2,
          dots: true,
          adaptiveHeight: true,
          responsive: [
            {
              breakpoint: 1240,
              settings: { slidesToShow: 3, slidesToScroll: 1 }
            },
            {
              breakpoint: 1024,
              settings: { slidesToShow: 2, slidesToScroll: 1 }
            },
            {
              breakpoint: 640,
              settings: { slidesToShow: 1, slidesToScroll: 1 }
            }
          ]
        });
      }
    }
  };

  /**
   * Admin theme dashboard behavior
   *
   * @type {{attach: function}}
   */
  behaviors.appDashboard = { attach: () => Dashboard.fadeToggle() };
})(jQuery, Drupal);
