import 'what-input';

(($, Drupal) => {
  const { behaviors } = Drupal;

  /**
   * Run foundation
   *
   * @type {{attach: function}}
   */
  behaviors.foundationInit = {
    attach: (context) => {
      $(context).foundation();
    }
  };
})(jQuery, Drupal);
