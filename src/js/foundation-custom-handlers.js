/**
 * @file
 * Foundation custom handlers
 */
((Foundation, CKEDITOR) => {
  Foundation.Abide.defaults.validators.editor = ($el, required, parent) => {
    required = $el.hasClass('required') || required;
    if (required) {
      const $sibling = $el.siblings().eq(0);
      if ($sibling.hasClass('cke')) {
        if (CKEDITOR.instances[$el.attr('id')].getData() === '') {
          parent.addClass('is-invalid-editor');
          return false;
        }
      }
    }
    parent.removeClass('is-invalid-editor');
    return true;
  };
})(Foundation, CKEDITOR);
