/**
 * External links library.
 *
 * @param {Object} options Library options.
 * @returns {(function(): void)|*} Return run function.
 *
 * @constructor
 */
const ExternalLinks = (options = {}) => {
  /**
   * Options.
   *
   * @type {*|{showIcons: boolean}}
   * @private
   */
  const _options = { ...ExternalLinks.defaults, ...options };

  /**
   * All link items.
   *
   * @private
   */
  const _links = document.getElementsByTagName('a');

  /**
   * If mail to url regex.
   *
   * @type {RegExp}
   * @private
   */
  const _mailtoRegex = /^mailto:/gm;

  /**
   * Clean the url to a simple domain without protocols.
   *
   * @param {String} url Url string.
   * @returns {String} Formatted url string.
   */
  const cleanDomain = (url) => {
    if (url === undefined) {
      return url;
    }
    // noinspection HttpUrlsUsage
    return url
      .replace('http://', '')
      .replace('https://', '')
      .split('/')[0]
      .split(':')[0];
  };

  /**
   * Adds event handlers for items within the ExternalLinks.
   *
   * @param {String} domain Current domain to compare if external.
   */
  const eventHandler = (domain) => {
    Array.from(_links).forEach((link) => {
      const cleanDomainVar = cleanDomain(link.href);
      if (
        cleanDomainVar.length &&
        domain !== cleanDomainVar &&
        _mailtoRegex.exec(link.href) === null
      ) {
        link.setAttribute('target', '_blank');
        if (_options.addExternalLinkClass) {
          link.classList.add(_options.externalLinkClassName);
        }
      }
    });
  };

  /**
   * Run.
   */
  return () => {
    eventHandler(cleanDomain(window.location.hostname));
  };
};

ExternalLinks.defaults = {
  /**
   * Enable or disable add external link class.
   *
   * @option
   * @type {boolean}
   * @default false
   */
  addExternalLinkClass: false,
  /**
   * The external link class name to set if addExternalLinkClass is true.
   *
   * @option
   * @type {string}
   * @default false
   */
  externalLinkClassName: 'external-link'
};

export default ExternalLinks;
