/**
 * @file
 * Replaced Drupal cores ajax throbber(s), see:
 *   https://www.drupal.org/node/2974681
 *
 */
(($, Drupal) => {
  Drupal.theme.ajaxProgressIndicatorFullscreen = () => `
    <div class='ajax-spinner ajax-spinner--fullscreen'>
      <div class='spinner-cn'>
        <div class='spinner'></div>
      </div>
    </div>`;
})(jQuery, Drupal);
