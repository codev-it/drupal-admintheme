import 'what-input';
import ExternalLinks from './lib/external-links';

/**
 * @file
 * Default app javascript
 */
(($, Drupal, window) => {
  const { behaviors, Ajax } = Drupal;

  /**
   * Replaced Drupal cores ajax throbber(s), see:
   * https://www.drupal.org/node/2974681
   */
  Ajax.prototype.setProgressIndicatorThrobber =
    Ajax.prototype.setProgressIndicatorFullscreen;

  /**
   * Run foundation
   *
   * @type {{attach: function}}
   */
  behaviors.adminFoundation = {
    attach: (context) => {
      $(context).once('foundation').foundation();
    }
  };

  /**
   * Run external links library
   *
   * @type {{attach: function}}
   */
  behaviors.adminExternalLinks = {
    attach: new ExternalLinks()
  };

  /**
   * Dialog overlay
   *
   * @type {{attach: function}}
   */
  behaviors.adminDialog = {
    attach: () => {
      $(window)
        .once('admin-dialog')
        .on({
          'dialog:beforecreate': (event, dialog, $element, settings) => {
            if (!$element.is('#drupal-off-canvas')) {
              const { dialogClass } = settings;
              const classes = dialogClass.split(' ');
              $('html').addClass('is-reveal-open');
              if (!classes.includes('reveal')) {
                classes.push('reveal');
                settings.dialogClass = classes.join(' ');
              }
            }
          },
          'dialog:afterclose': (event, dialog, $element) => {
            if (!$element.is('#drupal-off-canvas')) {
              $('html').removeClass('is-reveal-open');
            }
          }
        });
    }
  };

  /**
   * Foundation abide forms
   *
   * @type {{attach: function}}
   */
  behaviors.adminFormAbide = {
    attach: () => {
      // Form validation
      const $formAbide = $('form[data-abide]');
      if ($formAbide.length) {
        // noinspection SpellCheckingInspection
        $formAbide.once('adbide').on('forminvalid.zf.abide', (ev, frm) => {
          if (ev.namespace !== 'abide.zf') {
            return;
          }
          const $invalidFields = frm.find('[data-invalid]');
          if ($invalidFields.length) {
            const $firstElem = $invalidFields.eq(0);
            const scrollTo = $(`#${$firstElem.attr('id')}`).offset().top;
            const toolbarHeight =
              $('body').css('padding-top').replace('px', '') || 0;

            // open details
            $firstElem.parents('details').attr('open', 'open');

            // open field group tabs
            const $fieldGroupTabs = $firstElem.parents('.field-group-tab');
            if ($fieldGroupTabs.length) {
              $fieldGroupTabs.each((i, elem) => {
                const $button = $(
                  `.horizontal-tab-button a[href="#${elem.id}"]`
                );
                $button.click();
              });
            }

            // scroll to invalide elements
            $('html, body').animate(
              {
                scrollTop: scrollTo - toolbarHeight - 20
              },
              400
            );
          }
        });
      }
    }
  };
})(jQuery, Drupal, window);
