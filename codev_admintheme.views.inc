<?php

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: codev_admintheme.views.inc
 * .
 */

/**
 * Implements hook_form_BASE_FORM_ID_alter() for views_exposed_form.
 *
 * @noinspection PhpUnused
 * @noinspection PhpUnusedParameterInspection
 */
function codev_admintheme_form_views_exposed_form_alter(array &$form, FormStateInterface $form_state, $form_id) {
  $url_content = Url::fromRoute('system.admin_content');
  $url_media = Url::fromRoute('entity.media.collection');
  $url_redirect = Url::fromRoute('redirect.list');
  $url_redirect_404 = Url::fromRoute('redirect_404.fix_404');
  $lang_prefix = '<div class="hide">';
  $lang_suffix = '</div>';
  switch ($form['#action']) {
    case $url_content->toString():
    case $url_media->toString():
    case $url_redirect_404->toString():
    case $url_redirect->toString():
      // hide language selection if only one language active.
      $all_lang = Drupal::languageManager()->getLanguages();
      if (count($all_lang) == 1) {
        if (!empty($form['langcode'])) {
          $form['langcode']['#prefix'] = $lang_prefix;
          $form['langcode']['#suffix'] = $lang_suffix;
        }
        if (!empty($form['language'])) {
          $form['language']['#prefix'] = $lang_prefix;
          $form['language']['#suffix'] = $lang_suffix;
        }
      }
      break;
  }
}

/**
 * Implements hook_preprocess_views_view_table().
 *
 * @noinspection PhpUnused
 */
function codev_admintheme_preprocess_views_view_table(&$variables) {
  $view = $variables['view'];
  // hide language selection if only one language active.
  if ($view->id() == 'content') {
    $all_lang = Drupal::languageManager()->getLanguages();
    if (count($all_lang) == 1) {
      $variables['attributes'] = ['class' => ['no-lang']];
    }
  }
}

