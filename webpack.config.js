const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const FixStyleOnlyEntriesPlugin = require('webpack-fix-style-only-entries');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const WebpackShellPluginNext = require('webpack-shell-plugin-next');
const ExtraWatchWebpackPlugin = require('extra-watch-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const cssnanoPresetDefault = require('cssnano-preset-default');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const configLoader = require('./scripts/webpack/config-loader');
const {
  lookupPlaceholder,
  lookupPlaceholderMultiple
} = require('./scripts/webpack/placeholder');
const {
  buildWatchList,
  buildShellEvents
} = require('./scripts/webpack/builder');

// Load config
const {
  DIST,
  SCSS,
  JS,
  WATCH,
  SHELL,
  PURGE_CSS,
  PURGE_CSS_RERUN_AFTER_JS,
  PORT,
  PROXY,
  DEV_SERVER_PATH
} = configLoader();

module.exports = (env, options) => {
  if (DIST === undefined) {
    throw new Error('Config "DIST" ist required');
  }

  // Variables
  const assetsPath = lookupPlaceholder(DIST, false);
  const js = lookupPlaceholderMultiple(JS) || false;
  const scss = lookupPlaceholderMultiple(SCSS) || false;
  const watch = buildWatchList(WATCH) || {};
  const [shellJs, shellScss] = buildShellEvents(SHELL) || [];
  const purgeCssRerunAfterJs = !!PURGE_CSS && PURGE_CSS_RERUN_AFTER_JS;
  const devServerPath = lookupPlaceholder(DEV_SERVER_PATH) || false;

  // Default config
  // noinspection JSValidateTypes
  const defaultConfig = {
    mode: options.mode,
    devtool: 'source-map',
    plugins: [
      new BrowserSyncPlugin({
        proxy: PROXY,
        port: PORT
      })
    ],
    watchOptions: {
      ignored: '**/node_modules'
    }
  };

  if (devServerPath) {
    defaultConfig.devServer = {
      historyApiFallback: true,
      port: PORT
    };

    defaultConfig.plugins = [
      new HtmlWebPackPlugin({
        template: `${devServerPath}/index.html`,
        filename: 'index.html'
      })
    ];
  }

  // File config
  const fileConfig = [
    {
      test: /\.(eot|ttf|otf|woff|woff2)$/,
      loader: 'file-loader',
      options: {
        name: '[name].[ext]',
        outputPath: '../font'
      }
    },
    {
      test: /\.(svg|png|jpg|jpeg|gif|ico)$/,
      loader: 'file-loader',
      options: {
        name: '[name].[ext]',
        outputPath: '../img'
      }
    }
  ];

  // JS config
  // noinspection JSValidateTypes
  const jsConfig = {
    ...defaultConfig,
    name: 'js',
    entry: js,
    output: {
      path: `${assetsPath}/js`,
      filename: '[name].js',
      clean: !!devServerPath
    },
    resolve: {
      extensions: ['*', '.js', '.jsx']
    },
    externals: {
      jquery: 'jQuery'
    },
    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          exclude: /node_modules/,
          use: ['babel-loader', 'astroturf/loader']
        },
        {
          test: /\.(sa|sc|c)ss$/,
          use: ['style-loader', 'css-loader', 'postcss-loader', 'sass-loader']
        },
        ...fileConfig
      ]
    },
    plugins: [
      ...defaultConfig.plugins,
      new WebpackShellPluginNext(shellJs),
      new ExtraWatchWebpackPlugin(watch)
    ],
    optimization: { minimizer: [new UglifyJsPlugin()] }
  };

  // SCSS config
  // noinspection JSValidateTypes
  const scssConfig = {
    ...defaultConfig,
    name: 'scss',
    entry: scss,
    output: {
      path: `${assetsPath}/css`
    },
    resolve: {
      extensions: ['.scss']
    },
    module: {
      rules: [
        {
          test: /\.s[ac]ss$/i,
          use: [
            MiniCssExtractPlugin.loader,
            'css-loader',
            'postcss-loader',
            'sass-loader'
          ]
        },
        ...fileConfig
      ]
    },
    plugins: [
      ...defaultConfig.plugins,
      new MiniCssExtractPlugin({ filename: '[name].css' }),
      new FixStyleOnlyEntriesPlugin(),
      new WebpackShellPluginNext(shellScss),
      new ExtraWatchWebpackPlugin({
        ...watch,
        files: [].concat(
          watch.files || [],
          purgeCssRerunAfterJs ? [`${assetsPath}/**/*.js`] : []
        )
      })
    ],
    optimization: {
      minimizer: [
        new OptimizeCSSAssetsPlugin({
          cssProcessorPluginOptions: {
            preset: [
              cssnanoPresetDefault,
              {
                discardComments: { removeAll: true }
              }
            ]
          }
        })
      ]
    }
  };

  // Webpack config
  const webpackConfig = [];
  if (js) {
    webpackConfig.push(jsConfig);
  }
  if (scss) {
    webpackConfig.push(scssConfig);
  }
  return webpackConfig;
};
