<?php

use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Url;
use Drupal\search\SearchPageInterface;
use Drupal\views_ui\ViewUI;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: codev_admintheme.theme
 * .
 */

/**
 * Load parts
 */
require 'codev_admintheme.forms.inc';
require 'codev_admintheme.views.inc';

/**
 * Implements hook_preprocess_html().
 *
 * @noinspection PhpUnused
 */
function codev_admintheme_preprocess_html(&$variables) {
  // Add language body class.
  $variables['attributes']['class'][] = 'lang-' . Drupal::languageManager()
      ->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();
}

/**
 * Implements hook_preprocess_breadcrumb().
 *
 * @noinspection PhpUnused
 */
function codev_admintheme_preprocess_breadcrumb(&$variables) {
  $route_match = Drupal::routeMatch();
  $entity = $route_match->getParameter('entity');
  foreach ($variables['breadcrumb'] as $id => $item) {
    if (empty($item['text'])) {
      unset($variables['breadcrumb'][$id]);
    }
  }
  if ($entity instanceof SearchPageInterface) {
    $variables['current_page_title'] = $entity->getPlugin()->suggestedTitle();
  }
  else {
    $variables['current_page_title'] = Drupal::service('title_resolver')
      ->getTitle(Drupal::request(), $route_match->getRouteObject());
  }
  $variables['#cache']['contexts'][] = 'url';
}

/**
 * Implements hook_preprocess_links().
 *
 * @noinspection PhpUnused
 */
function codev_admintheme_preprocess_links(&$variables) {
  $links = $variables['links'] ?? [];
  switch ($variables['theme_hook_original']) {
    case 'links__dropbutton':
    case 'links__dropbutton__operations':
      $first_button = array_values($links)[0]['link'] ?? [];
      $first_button['#options']['attributes']['class'][] = 'button';
      $first_button['#options']['attributes']['class'][] = 'tiny';
      $first_button['#options']['attributes']['class'][] = 'margin-0';
      $variables['dropdown_id'] = uniqid();
      $variables['first_button'] = $first_button;
      break;
  }
}

/**
 * Implements hook_preprocess_admin_block_content().
 *
 * @noinspection PhpUnused
 */
function codev_admintheme_preprocess_admin_block_content(&$variables) {
  $content = $variables['content'] ?? [];
  $user = Drupal::currentUser()->getAccount();
  foreach ($content as $key => $item) {
    if (!empty($item['url']) && !empty($item['link'])) {
      $url = $item['url'];
      if (!$url instanceof Url) {
        if (!preg_match('/^http(|s):\/\//m', $url)) {
          $url = str_replace('//', '/', 'internal:' . $url);
        }
        $url = Url::fromUri($url);
      }
      if ($url instanceof Url && !$url->access($user)) {
        unset($variables['content'][$key]);
      }
    }
  }
}

/**
 * Implements hook_views_ui_display_tab_alter().
 *
 * @noinspection PhpUnused
 * @noinspection PhpUnusedParameterInspection
 */
function codev_admintheme_views_ui_display_tab_alter(&$build, ViewUI $view, $display_id) {
  $uniq_id = uniqid();
  $path = $build['details']['top']['actions']['path'] ?? [];
  unset($path['#prefix'], $path['#suffix']);
  $path['#attributes']['class'][] = 'button';
  $path['#attributes']['class'][] = 'tiny';
  $dropdown_btn = sprintf('<a class="dropdown button arrow-only" data-toggle="%s"><span class="show-for-sr">Show menu</span></a>', $uniq_id);
  $wrapper_div = sprintf('<div id="%s" class="dropdown-pane padding-0" data-dropdown data-hover="true" data-hover-pane="true">', $uniq_id);
  $first_btn_markup = Drupal::service('renderer')->render($path);
  $build['details']['top']['actions']['prefix']['#markup'] = sprintf(
    '<div class="button-group margin-0">%s%s%s<ul class="dropbutton menu vertical">',
    $first_btn_markup, $dropdown_btn, $wrapper_div);
  $build['details']['top']['actions']['suffix']['#markup'] = '</ul></div></div>';
}
